import logo from './logo.svg';
import './App.css';
import Card from './components/Card';

function App() {
  return (
    <div className='main'>
      <Card name='Lekau Mamabolo' quote='i am learning'/>
      <Card name='Rick Ross ' quote='All I do is win'/>
      <Card name='Jane Doe' quote='Take care of yourself'/>
      <Card name='Drake Graham' quote='You only Live once'/>
    </div>
  );
}

export default App;
