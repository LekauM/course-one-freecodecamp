import profile from '../images/profile.jpg'
export default function Card(name){
    return(
        <div className='card'>
            <img alt='profile' src={profile} className='profile'></img>
            <div className='container'>
                <h3>{name.name}</h3>
                <p>{name.quote}</p>
            </div>
        </div>
    );
}