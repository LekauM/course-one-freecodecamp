export default function Joke(props){
    return(
        <div className='joke-container'>
            <h4>{props.setup}</h4>
            <h3>{props.punchline}</h3>
        </div>
    )
}