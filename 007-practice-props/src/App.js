import './App.css';
import Joke from './components/joke';

function App() {
  return (
    <div>
      <Joke 
        setup="Postal service jokes don't need much setup" 
        punchline="It's all in the delivery"/>
      <Joke 
        setup="What do you get when you switch the setup and punch line?" 
        punchline="An unfunny joke."/>
      <Joke 
        setup="What's the most important part of a joke, the setup or the punchline?" 
        punchline='To get to the other side.'/>
      <Joke 
        setup="Earth, Venus, Mars, and Jupiter were going to setup a party" 
        punchline="But they failed because nobody knew how to planet"/>
    </div>
  );
}

export default App;
