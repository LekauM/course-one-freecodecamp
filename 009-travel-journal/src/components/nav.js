import world  from "../images/World.png";
export default function Nav(){
   return ( 
    <nav>
        <img src={world} alt='world'></img>
        <h3>My Travel Journal</h3>
    </nav>
   );
}