import pin from '../images/pin.png'
export default function DestinationCard(props){
   return (
    <div className='card'>
        <img src={props.imageUrl} alt=''></img>
        <div className='details'>
            <span><img src={pin}alt='pin'></img>  {props.location} <a href={props.googleMapsUrl}>View on Google Maps</a></span>
            <h2>{props.title}</h2>
            <h4>{props.startDate} - {props.endDate}</h4>
            <p>{props.description}</p>
        </div>
    </div>
   );
}