import DestinationCard from "./destinationCard";
import {destinations} from "../dummy_data";
export default function DestinationCards(){
    const destinationItems = destinations.map(function(destination){
        return (
            <DestinationCard 
                id={destination.id}
                {...destination}
            />
        );
    });
    return ( 
        <div className='body'>
            {destinationItems}
        </div>
    );
}