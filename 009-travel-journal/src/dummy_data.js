const destinations = [
    {
        id: 0,
        title:"AfriSki",
        location:"Lesotho",
        googleMapsUrl:"https://goo.gl/maps/brzC5pAX6GouzJNz7",
        startDate:"12 Jun, 2021",
        endDate:"19 Jun, 2021",
        description:"Afriski is the only skiing resort in Lesotho, located 3050 m above sea-level in the Maluti Mountains, operating in Southern Africa near the northern border of Lesotho with South Africa. It is one of only two ski resorts in southern Africa",
        imageUrl:"https://i2.wp.com/www.5starstories.co/wp-content/uploads/2018/04/afriski-mountain-resort-lesotho.jpg?fit=800%2C490&ssl=1"
    },
    {
        id: 1,
        title:"FIJI",
        location:"Melanesia",
        googleMapsUrl:"https://goo.gl/maps/Wb8Vt3qMTdHdhbtr6",
        startDate:"01 Mar, 2021",
        endDate:"07 Mar, 2021",
        description:"Fiji officially the Republic of Fiji, is an island country in Melanesia, part of Oceania in the South Pacific Ocean. It lies about 1,100 nautical mile",
        imageUrl:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQQKSTUC53Hl82N50ZcpKnLQeaDqoz_UNGxLr-tpe-sqLPhA8QUFqZIfikAA1WbhsuK0o&usqp=CAU"
    },
    {
        id: 2,
        title:"Male",
        location:"Maldives",
        googleMapsUrl:"https://goo.gl/maps/aRdokjMvoa7QBpjY8",
        startDate:"15 Dec, 2019",
        endDate:"22 Dec, 2019",
        description:"Malé is the densely populated capital of the Maldives, an island nation in the Indian Ocean. It's known for its mosques and colorful buildings.",
        imageUrl:"https://lp-cms-production.imgix.net/2019-06/GettyImages-156724803_high.jpg"
    },
    {
        id: 3,
        title:"Mount Fuji",
        location:"Japan",
        googleMapsUrl:"https://goo.gl/maps/52o2uzDs6H66qwU18",
        startDate:"17 Aug, 2019",
        endDate:"24 Aug, 2019",
        description:"Japan’s Mt. Fuji is an active volcano about 100 kilometers southwest of Tokyo. Commonly called “Fuji-san,” it’s the country’s tallest peak, at 3,776 meters.",
        imageUrl:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT0z1ZdFJp7PvdvauCyDzKQsrIn-1bf-_VrofPTNQRLSpyqQfJySft0uHZ_tQ-olQS2PzY&usqp=CAU"
    }
]

export {destinations};