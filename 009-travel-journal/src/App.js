import './App.css';
import Nav from './components/nav';
import DestinationCards from './components/destinationCards';

function App() {
  return (
    <div className='main'>
      <Nav />
      <DestinationCards />
    </div>
  );
}

export default App;
