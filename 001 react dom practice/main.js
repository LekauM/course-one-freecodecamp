function MainContent(){
    return (<div>
        <h1>This is the main content custom react component</h1>
    </div>)
}

ReactDOM.render(
    <MainContent />,
    document.getElementById('root')
);