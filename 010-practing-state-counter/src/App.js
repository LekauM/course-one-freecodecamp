import './App.css';
import React, {useState} from 'react'
function App() {
  const [number, setNumber] = useState(0);

  function onClickPlusHandler(){
    setNumber(function(num){
      return num + 1;
    });
  }

  function onClickMinusHandler(){
    setNumber(function(num){
      return num - 1;
    });
  }
  return (
    <div className='main'>
      <h1>{number}</h1>
      <span className='buttons'>
        <button onClick={onClickMinusHandler}>-</button>
        <button onClick={onClickPlusHandler}>+</button>
      </span>
    </div>
  );
}

export default App;
