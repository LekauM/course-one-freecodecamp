export default function Footer(){
    return(
        <footer className="footer">
            <small>2022 Mamabolo Development Company, All Rights Reserved</small>
        </footer>
    )
}