const nums = [1,2,3,4,5]
const names = ['lekau', 'daniel', 'kerry', 'tim']
const food = ['kota', 'sushi', 'hotdog', 'fish']

const numsSq = nums.map(function(num){
    return num * num;
})

const capNames = names.map(function(name){
    return name.charAt(0).toUpperCase() + name.substring(1);
})

const pTagFood = food.map(function(food){
    return `<p>${food}<p/>`
})

console.log(numsSq);
console.log(capNames);
console.log(pTagFood);