import joker from '../joker.jpeg'
export default function Info(){
    return(
        <header className='header'>
            <img src={joker} alt='profile_picture' className='profile-picture'></img>
            <h1>Lekau Mamabolo</h1>
            <h4>Aspiring React Developer</h4>
            <p>Something i cant read</p>
            <div className='buttons'>
                <button className='email-button'>Email</button>
                <button className='linkedin-button'>LinkedIn</button>
            </div>
        </header>
    )
}