import { FaFacebookSquare } from "react-icons/fa"
import { FaTwitterSquare } from "react-icons/fa"
import { FaInstagramSquare } from "react-icons/fa"
import { FaGithubSquare } from "react-icons/fa"
export default function Footer(){
    return (
        <footer className='footer'>
            <div>
                <FaTwitterSquare />
            </div>
            <div>
                <FaFacebookSquare />
            </div>
            <div>
                <FaInstagramSquare />
            </div>
            <div>
                <FaGithubSquare />
            </div>
        </footer>
    )
}