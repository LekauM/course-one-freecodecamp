import './App.css';

function App() {
  const firstName = 'Lekau';
  const lastName = 'Mamabolo';
  const date = new Date();
  const hours = date.getHours();
  const minutes = date.getMinutes();
  var partOfDay = ''
  if (hours < 12){
    partOfDay = 'Morning';
  } else if (hours < 17) {
    partOfDay = 'afternoon'
  } else {
    partOfDay = 'evening'
  }
  return (
    <div className="App">
      <h1>Good {partOfDay} {firstName} {lastName}!</h1> 
      <h2>it is currently {hours}:{minutes} </h2>
    </div>
  );
}

export default App;
