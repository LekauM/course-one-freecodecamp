import hero from '../images/airbnb_hero.png'
export default function Hero(){
    return(
        <div className='hero'>
            <img src={hero} alt='hero'></img>
            <div className='hero-text'>
                <h1>Online Experience</h1>
                <h2>
                    Join unique interactive activities led by one-of-a-kind 
                    hosts—all without leaving home.
                </h2>
            </div> 
        </div>
    );
}