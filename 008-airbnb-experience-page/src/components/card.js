import star from "../images/Star 1.png"
export default function Card(props){
    return(
        <div className='card'>
            <img src={props.image} alt='card'></img>
            <div className='card-stats'>
                <span><img src={star} alt='star'></img></span>
                <span> {props.rating} ({props.numberOfRaters}) - </span>
                <span>{props.country}</span>
            </div>
            <p>{props.title}</p>
            <h4> From R{props.price} /person</h4>
        </div>
    );
}