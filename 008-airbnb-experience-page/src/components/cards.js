import Card from "./card";
import {experiences} from "../data"
export default function Cards(){
    const cardDetails = experiences.map(function(experience){
        return (<Card 
           id={experience.id}
           {...experience}
        />);
    })
    return(
        <div className='cards'>
           {cardDetails}
        </div>
    );
}