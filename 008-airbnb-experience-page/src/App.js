import './App.css';
import Navbar from './components/navbar';
import Hero from './components/hero';
import Cards from './components/cards';

function App() {
  return (
    <div>
      <Navbar />
      <Hero />
      <Cards />
    </div>
  );
}

export default App;
