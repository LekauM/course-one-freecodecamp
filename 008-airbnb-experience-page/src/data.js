const experiences = [
    {
        'id': 0,
        'image': "images/image0.png",
        'rating': 4.5,
        'numberOfRaters': 9,
        'country': "South Africa",
        'title': "Swimming Lessons with Penny",
        'price': 499.99,
        'itemsLeft': 5
    },
    {
        'id': 1,
        'image': "images/image0.png",
        'rating': 4.2,
        'numberOfRaters': 6,
        'country': "South Africa",
        'title': "Swimming Lessons with Lekau",
        'price': 399.99,
        'itemsLeft': 0
    },
    {
        'id': 2,
        'image': "images/image0.png",
        'rating': 1.5,
        'numberOfRaters': 15,
        'country': "South Africa",
        'title': "Swimming Lessons with Gold fish",
        'price': 98.99,
        'itemsLeft': 5
    },
]

export {experiences}