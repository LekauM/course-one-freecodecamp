function Header(){
    return(
        <header>
            <nav className='nav'>
                <img src='./react-logo.png' width='100'></img>
                <ul className='nav-item'>
                    <li>Pricing</li>
                    <li>About</li>
                    <li>Contact</li>
                </ul>
            </nav>
        </header>
    )
}

function MainContent(){
    return(
        <div>
            <h1>Reasons I'm excited to learn React</h1>
            <ol>
                <li>I can plug it to normal HTML sites</li>
                <li>Its complicated so its fun</li>
                <li>I will need it</li>
            </ol>
        </div> 
    )
}

function Footer(){
    return(
         <footer>
             <small>
                20xx Mamabolo development. All rights reserved
            </small>
        </footer>
    )
}
function Page(){
    return(
        <div>
            <Header />
            <MainContent />
            <Footer />   
        </div>
    )
}

ReactDOM.render(<Page />, document.getElementById('root'));